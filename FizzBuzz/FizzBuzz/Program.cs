﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        private const int LENGTH = 100;
        private const int FIZZ = 3;
        private const int BUZZ = 5;

        static void Main(string[] args)
        {
            for (int i = 1; i <= LENGTH; i++) Console.WriteLine(GetFizzBuzz(i));
            
            Console.ReadLine(); //Wait for key press to close
        }

        private static object GetFizzBuzz(int i)
        {
            if (i % FIZZ == 0 && i % BUZZ == 0) return "FizzBuzz";
            else if (i % FIZZ == 0) return "Fizz";
            else if (i % BUZZ == 0) return "Buzz";
            else return i;
        }
    }
}
